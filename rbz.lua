rlf = require "rlf"

local bot = {
	rot = 0, -- Right, down, left, up 0-3
	x = 0, -- Current X value
	y = 0, -- Current Y value
	fail = "", -- Indicates if we have failed
	cur_func = "F1", -- Currently executing function
	stack = {}, -- Function stack for recursion
	ran = 0, -- Contains how many instructions were ran
	stars = 0
}

local rot = {
	up = 3,
	down = 1,
	left = 2,
	right = 0
}

local map = {
	-- The map is 12x16 squares
	-- Format:
	-- ' ' for nothing,
	-- 'r' for red,
	-- 'g' for green,
	-- 'b' for blue,
	-- the same but uppercase for stars
	tiles = {},
	-- Number of stars in the level, filled automatically by the init function
	stars = 0,

	-- Title of the puzzle
	title = "",

	-- Column, row and rotation that works as the bot spawner
	start = {x = 1, y = 1, rot = rot.right}
}

funcs = {
	F1 = "_F_1"
}

function GetTile(x, y)
	if map.tiles[y] == nil then return {type = "#", color = "R"} end

	local t = (map.tiles[y]):sub(x, x)
	local tmp = ({
		[" "] = "#";
		["r"] = ".";
		["g"] = ".";
		["b"] = ".";
		["R"] = "*";
		["G"] = "*";
		["B"] = "*";
	})[t]

	return {
		type = tmp,
		color = ((map.tiles[y]):sub(x, x)):upper()
	}
end

function SetTile(x, y, tile)
	c = GetTile(x, y).color

	if tile == " " then 
		map.tiles[y] = map.tiles[y]:sub(1, x-1) .. " " .. map.tiles[y]:sub(x+1)
		return
	end

	if tile == "." then
		map.tiles[y] = map.tiles[y]:sub(1, x-1) ..
		(({
			["r"] = "r";
			["g"] = "g";
			["b"] = "b";
			["R"] = "r";
			["G"] = "g";
			["B"] = "b";
		})[c])
		.. map.tiles[y]:sub(x+1)
		return
	end

	if tile == "*" then
		map.tiles[y] = map.tiles[y]:sub(1, x-1) ..
		(({
			["r"] = "R";
			["g"] = "G";
			["b"] = "B";
			["R"] = "R";
			["G"] = "G";
			["B"] = "B";
		})[c])
		.. map.tiles[y]:sub(x+1)
		return
	end

	map.tiles[y] = map.tiles[y]:sub(1, x-1).. tile ..map.tiles[y]:sub(x+1)
end

function SetColor(x, y, color)
	t = GetTile(x, y).type

	if (t == "#") then return end

	if (t == ".") then
		map.tiles[y] = map.tiles[y]:sub(1, x-1).. color:upper() ..map.tiles[y]:sub(x+1)
	else
		map.tiles[y] = map.tiles[y]:sub(1, x-1).. color:lower() ..map.tiles[y]:sub(x+1)
	end
end

function CanBeIn(x, y)
	t = GetTile(x, y).type
	return t ~= "#" and t ~= ""
end

function BotForward()
	(({
		-- Right
		[rot.right] = (function() bot.x = bot.x + 1 end);
		-- Down
		[rot.down] = (function () bot.y = bot.y + 1 end);
		-- Left
		[rot.left] = (function() bot.x = bot.x - 1 end);
		-- Up
		[rot.up] = (function() bot.y = bot.y - 1 end);
	})[bot.rot])()

	if (not CanBeIn(bot.x, bot.y)) then
		bot.lost = "Fell to /dev/null"
	end
end

function BotRight()
	bot.rot = ({
		-- Right -> Down
		[rot.right] = rot.down;
		-- Down -> Left
		[rot.down] = rot.left;
		-- Left -> Up
		[rot.left] = rot.up;
		-- Up -> Right
		[rot.up] = rot.right;
	})[bot.rot]
end

function BotLeft()
	bot.rot = ({
		-- Right -> Up
		[rot.right] = rot.up;
		-- Down -> Right
		[rot.down] = rot.right;
		-- Left -> Down
		[rot.left] = rot.down;
		-- Up -> Left
		[rot.up] = rot.left;
	})[bot.rot]
end

function ExecInstruction(num)
	local inst = funcs[bot.cur_func]
	local cond = inst:sub(num, num)
	local action = inst:sub(num+1, num+1)
	local changed_func = false

	if (GetTile(bot.x, bot.y).color == cond or cond == "_") then
		(({
			["F"] = BotForward;
			["R"] = BotRight;
			["L"] = BotLeft;
			["r"] = function() SetColor(bot.x, bot.y, "R") end;
			["g"] = function() SetColor(bot.x, bot.y, "G") end;
			["b"] = function() SetColor(bot.x, bot.y, "B") end;
			["1"] = function() bot.cur_func = "F1" table.insert(bot.stack, {"F1", num}) changed_func = true end;
			["2"] = function() bot.cur_func = "F2" table.insert(bot.stack, {"F2", num}) changed_func = true end;
			["3"] = function() bot.cur_func = "F3" table.insert(bot.stack, {"F3", num}) changed_func = true end;
			["4"] = function() bot.cur_func = "F4" table.insert(bot.stack, {"F4", num}) changed_func = true end;
			["5"] = function() bot.cur_func = "F5" table.insert(bot.stack, {"F5", num}) changed_func = true end;
			["_"] = function() bot.ran = bot.ran - 1 end
		})[action])()

		if (GetTile(bot.x, bot.y).type == "*") then
			SetTile(bot.x, bot.y, ".")
			bot.stars = bot.stars + 1
		end

		bot.ran = bot.ran + 1
	end

	return changed_func
end

function DrawMap()
	for y=1, #map.tiles do
		for x=1, #map.tiles[y] do
			if (GetTile(x, y).type ~= "#") then
				io.write(({
					["R"] = "\027[31m"; -- Red color
					["G"] = "\027[32m"; -- Green color
					["B"] = "\027[34m"; -- Blue color
				})[GetTile(x, y).color])
				if (bot.y == y and bot.x == x) then
					io.write(({
						[rot.left] = "<";
						[rot.right] = ">";
						[rot.up] = "^";
						[rot.down] = "v";
					})[bot.rot])	
				else
					io.write(GetTile(x, y).type)
				end
				io.write("\027[0m")
			else
				io.write(" ")
			end
		end
		io.write("\n")
	end
end

function DrawFunctions(inst_n)
	for name,fn in pairs(funcs) do
		io.write(tostring(name)..": ")

		for k,v in ipairs(fn) do
			io.write(" ")

			if (inst_n == k and bot.cur_func == name) then
				io.write("[")
			end

			io.write(({
				["R"] = "\027[31m"; -- Red color
				["G"] = "\027[32m"; -- Green color
				["B"] = "\027[34m"; -- Blue color
				["_"] = "\027[37m"; -- Normal color
			})[v:sub(1, 1)])

			io.write(({
				["F"] = "Forward";
				["R"] = "Right";
				["L"] = "Left";
				["r"] = "Paint red";
				["g"] = "Paint green";
				["b"] = "Paint blue";
				["_"] = "Do nothing";
				["1"] = "F1";
				["2"] = "F2";
				["3"] = "F3";
				["4"] = "F4";
				["5"] = "F5";
			})[v:sub(2, 2)])

			io.write("\027[0m")

			if (inst_n == k and bot.cur_func == name) then
			io.write("]")
			end

			io.write(" ")
		end
		io.write("\n")
	end
end

function Init()
	bot.x = map.start.x
	bot.y = map.start.y
	bot.rot = map.start.rot
	
	for y=1, #map.tiles do
		for x=1, #map.tiles[y] do
			if (GetTile(x, y).type == "*") then
				map.stars = map.stars + 1
			end
		end
	end

	-- Clear screen
	io.write("\027[2J")
	io.write("\027[3J")
end

function ClearDrawings()
	-- Clear a line, many times
	for i=1, (#map.tiles + 3) do
		io.write("\027[1A")
		io.write("\027[0K")
	end
end

do
	if arg[1] == nil then
		io.write("Usage: rbz puzzle-id\n")
		os.exit(1)
	end

	local lvl = rlf.fetch(arg[1])
	
	if lvl == 1 then
		os.exit(1)
	end

	map.tiles = lvl.tiles
	map.start.x = lvl.robotCol + 1
	map.start.y = lvl.robotRow + 1
	map.start.rot = lvl.robotDir
	map.title = lvl.title
	p(lvl)
end

Init()
ClearDrawings()
io.write("Puzzle "..arg[1]..": "..map.title.."\n")
DrawMap()

io.write("F1: ")
funcs.F1 = io.read("*l")

io.write("F2: ")
funcs.F2 = io.read("*l")

io.write("F3: ")
funcs.F3 = io.read("*l")

io.write("F4: ")
funcs.F4 = io.read("*l")

io.write("F5: ")
funcs.F5 = io.read("*l")

io.write("F6: ")
funcs.F6 = io.read("*l")

local inst_num = 1

while inst_num > #funcs[bot.cur_func] do
	ClearDrawings()
	io.write("Puzzle "..arg[1]..": "..map.title.."\n")
	DrawMap()
	DrawFunctions(inst_num)

	io.write(bot.stars, " out of ", map.stars, " stars\n")

	if (ExecInstruction(inst_num)) then inst_num = 0 end

	if (bot.stars == map.stars) then bot.lost = "Success" end
	if (bot.ran >= 1000) then bot.lost = "Stack Overflow" end
	if (bot.lost ~= "" and bot.lost ~= nil) then ClearDrawings() DrawMap() break end

	inst_num = inst_num + 2

	local start = os.clock()
	repeat until os.clock() > start + 0.1
end

if bot.lost == "" or bot.lost == nil then bot.lost = "No more instructions to execute" end

print(bot.lost.."!")
os.exit(1)

