#!/usr/bin/lua

local rlf = {}

local function S(cmd)
	local run = io.popen(cmd)
	local out = run:read("*a")
	run:close()
	run = nil
	return out
end

function rlf.fetch(level_id)
	base = 'http://robozzle.com/js/play.aspx?puzzle='

	id = level_id

	if id == nil then
		io.stderr:write("Usage:\n\trbz puzzle_id\n")
		return 1
	end

	url = base..tostring(id)

	io.write("Fetching '", url, "'\n")

	html = S("curl -s '"..url.."'")

	info = html:match "var puzzles = %[([^;]+)%]"

	if info == nil then
		io.stderr:write("Error: Puzzle not found!\nUsage:\n\trbz puzzle_id\n")
		return 1
	end

	local level = {}

	level.title = info:match 'title:"([a-zA-Z %.!;]+)"'

	level.board = info:match 'board:"([ rgbRGB]+)"'

	level.robotCol = tonumber(info:match('robotCol:(%d+)'))

	level.robotRow = tonumber(info:match('robotRow:(%d+)'))

	level.robotDir = tonumber(info:match('robotDir:(%d+)'))

	level.allowedCommands = tonumber(info:match('allowedCommands:(%d)'))

	level.subs = {info:match 'subs:%[(%d+),(%d+),(%d+),(%d+),(%d+)]'}

	for i=1, #level.subs do
		level.subs[i] = tonumber(level.subs[i])
	end

	do
		local w, h = html:match 'robozzle.CreateBoard%((%d+), (%d+), %d+, %d+%);'
		level.width = tonumber(w)
		level.height = tonumber(h)
	end

	do
		local tiles = {}
		for pos = 1, level.height do
			tiles[pos] = level.board:sub((pos-1) * level.width+1, pos * level.width)
		end

		level.tiles = tiles
	end

	level.board = nil

	return level
end

return rlf
